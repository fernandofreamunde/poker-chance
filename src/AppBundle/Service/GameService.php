<?php
/**
 * Created by PhpStorm.
 * User: fernando
 * Date: 26-06-2018
 * Time: 21:19
 */

namespace AppBundle\Service;


use AppBundle\ValueObject\Card;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class GameService
{
    private $chosenCard;

    private $session;

    /**
     * @var DeckService
     */
    private $deck;

    private $gameStarted;

    public function __construct(SessionInterface $session, DeckService $deckService)
    {
        $this->session = $session;

        if ($this->session->get('chosenCard')) {
            $this->chosenCard = $this->session->get('chosenCard');
        }

        $this->deck = $deckService;

        $this->gameStarted = false;
        if ($this->session->get('game_started'))
        {
            $this->gameStarted = true;
        }
    }

    public function setChosenCard(Request $request)
    {
        if ($request->get('suit') == null ||  $request->get('rank') == null) {
            return false;
        }

        $this->chosenCard = new Card($request->get('suit'), $request->get('rank'));
        $this->session->set('chosenCard', $this->chosenCard);
    }

    public function getProbability($endGame = false)
    {
        $cardCount = $this->deck->count();
        if ($endGame) {
            $cardCount++;
        }

        // no point in preventing division by 0 that should never happen
        return 1 / $cardCount * 100;
    }

    public function play()
    {
        $drawnCard = $this->deck->draw();

        if ($drawnCard != $this->chosenCard) {
            return $drawnCard;
        }

        return false;
    }

    public function getChosenCard()
    {
        return $this->chosenCard;
    }

    public function hasGameStarted()
    {
        return $this->gameStarted;
    }

    public function start()
    {
        $this->gameStarted = true;
        $this->session->set('game_started', $this->gameStarted);
    }
}