<?php
/**
 * Created by PhpStorm.
 * User: fernando
 * Date: 24-06-2018
 * Time: 21:15
 */

namespace AppBundle\Service;


use ArrayObject;
use AppBundle\ValueObject\Card;
use AppBundle\ValueObject\Ranks;
use AppBundle\ValueObject\Suits;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class DeckService
{
    private $deck;

    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;

        if (!$this->session->get('deck')) {

            $suits = [Suits::DIAMONDS, Suits::SPADES, Suits::HEARTS, Suits::CLUBS ];
            $ranks = [Ranks::ACE, Ranks::TWO, Ranks::THREE, Ranks::FOUR, Ranks::FIVE, Ranks::SIX, Ranks::SEVEN, Ranks::EIGHT, Ranks::NINE, Ranks::TEN, Ranks::QUEEN, Ranks::JACK, Ranks::KING];

            $deck = [];

            foreach ($suits as $suit) {
                foreach ($ranks as $rank) {
                    $deck[] = new Card($suit, $rank);
                }
            }
            shuffle($deck);
            $this->deck = new ArrayObject($deck, ArrayObject::ARRAY_AS_PROPS);

        } else {
            $this->deck = $this->session->get('deck');
        }

    }

    public function count()
    {
        return count($this->deck);
    }

    public function draw()
    {
        $card = $this->deck[0];
        $this->deck->offsetUnset(0);
        $deck = (Array) $this->deck;
        $deck = array_values($deck);
        $this->deck = new ArrayObject($deck, ArrayObject::ARRAY_AS_PROPS);

        $this->session->set('deck', $this->deck);

        return $card;
    }
}