<?php
/**
 * Created by PhpStorm.
 * User: fernando
 * Date: 24-06-2018
 * Time: 20:52
 */

namespace AppBundle\ValueObject;


class Card
{
    private $suit;
    private $rank;

    public function __construct($suit, $rank)
    {
        $this->suit = $suit;
        $this->rank = $rank;
    }

    public function show()
    {
        return $this->suit . $this->rank;
    }
}