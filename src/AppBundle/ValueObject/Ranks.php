<?php
/**
 * Created by PhpStorm.
 * User: fernando
 * Date: 24-06-2018
 * Time: 20:54
 */

namespace AppBundle\ValueObject;


class Ranks
{
    const ACE   = 'A';
    const KING  = 'K';
    const QUEEN = 'Q';
    const JACK  = 'J';
    const TEN   = '10';
    const NINE  = '9';
    const EIGHT = '8';
    const SEVEN = '7';
    const SIX   = '6';
    const FIVE  = '5';
    const FOUR  = '4';
    const THREE = '3';
    const TWO   = '2';
}