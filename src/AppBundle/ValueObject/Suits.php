<?php
/**
 * Created by PhpStorm.
 * User: fernando
 * Date: 24-06-2018
 * Time: 20:57
 */

namespace AppBundle\ValueObject;


class Suits
{
    const HEARTS   = 'H';
    const CLUBS    = 'C';
    const DIAMONDS = 'D';
    const SPADES   = 'S';
}