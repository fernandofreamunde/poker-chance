<?php

namespace AppBundle\Controller;

use AppBundle\Service\GameService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class CardGameController extends Controller
{
    private $session;

    /**
     * @var GameService
     */
    private $game;

    public function __construct(GameService $gameService)
    {
        $this->session = new Session();
        $this->game = $gameService;
    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('game/index.html.twig');
    }

    /**
     * @Route("/restart", name="restart")
     */
    public function restartAction()
    {
        $this->session->clear();
        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/card", name="card")
     * @Method({"POST"})
     */
    public function cardAction(Request $request)
    {
        if($this->game->setChosenCard($request)) {
            return $this->redirectToRoute('homepage');
        }
        return $this->redirectToRoute('game');
    }

    /**
     * @Route("/game", name="game")
     */
    public function gameAction()
    {
        if (!$this->session->get('chosenCard'))
        {
            return $this->redirectToRoute('homepage');
        }

        if (!$this->game->hasGameStarted()) {
            $this->game->start();
            return $this->render('game/run.html.twig', [
                'probability' => $this->game->getProbability(),
                'card' => $this->game->getChosenCard()->show(),
                'drawn_card' => null,
            ]);
        }

        $drawnCard = $this->game->play();

        if (!$drawnCard) {
            return $this->redirectToRoute('end');
        }

        return $this->render('game/run.html.twig', [
            'probability' => $this->game->getProbability(),
            'card' => $this->game->getChosenCard()->show(),
            'drawn_card' => $drawnCard->show(),
        ]);
    }

    /**
     * @Route("/end", name="end")
     */
    public function endAction()
    {
        if (!$this->session->get('chosenCard'))
        {
            return $this->redirectToRoute('homepage');
        }

        return $this->render('game/end.html.twig', [
            'probability' => $this->game->getProbability(true),
            'card' => $this->game->getChosenCard()->show(),
        ]);
    }
}
