<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Service\DeckService;
use AppBundle\ValueObject\Card;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;

class DeckServiceTest extends WebTestCase
{
    public function testGetProbability()
    {
        $session = new Session(new MockArraySessionStorage());

        $deckService = new DeckService($session);

        $this->assertEquals(52, $deckService->count());
        $this->assertTrue( $deckService->draw() instanceof Card);
        $this->assertEquals(51, $deckService->count());
    }
}
