<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Service\DeckService;
use AppBundle\Service\GameService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;

class GameServiceTest extends WebTestCase
{
    public function testGetProbability()
    {
        $session = new Session(new MockArraySessionStorage());

        $deckService = new DeckService($session);

        $gameService = new GameService($session, $deckService);

        $firstProbability = 1/52*100;

        $this->assertEquals($firstProbability, $gameService->getProbability());

        $gameService->play();
        $this->assertEquals(1/51*100, $gameService->getProbability());

        $this->assertNotEquals($firstProbability, $gameService->getProbability());
    }
}
